<?php 

session_start();

class TaskList{

	// Add Task
	public function add($description){
		// Create a $newTask object with the description and its status.
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		// Check if the 'tasks' session variable exists, and create an empty numeric array.
		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		// Add the new task to the "$_SESSION['tasks']" array.
		array_push($_SESSION['tasks'], $newTask);
	}

	// Update Task
	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
	}

	// Delete Task
	public function remove($id){
		// Syntax: array_splice(array, startDel, length, newArrayElement)
		array_splice($_SESSION['tasks'], $id, 1);
	}

	// Clear all taks
	public function clear(){
		// This destroy or terminate all the active session.
		session_destroy();
	}
}

// Instantiation of TaskList
$taskList = new TaskList();

// Handle the client request and identify the "action" needed to be executed.
// Once the action matches the condition, it will invoke a specific method.
if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']);
} else if($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
} else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
} else if($_POST['action'] === 'clear'){
	$taskList->clear();
}

// This is only used to display session's contents for debugging purpose.
// var_dump($_SESSION);

// This is to redirect the user's browser to the index.php after server processing.
header('Location: ./index.php');


?>