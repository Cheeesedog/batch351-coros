<?php 
	
	// [SECTION] Super Global Variables
	// Superglobal Variables in PHP are predefined "global variables", which means that they can be used whenever needed.
	// Superglobal variables are written with "$" followed by an "_" abd UPPERCASE LETTER

	// $_GET and $_POST are examples of super global variables in PHP.

	// has data input in the form of GET requests.
	// echo '$GET: <br/>';
	// var_dump($_GET);

	// echo '<br/> <br/>';

	// stores data input in the form of POST requests.
	// echo '$POST: <br/>';
	// var_dump($_POST);

	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	// "isset()" is a built-in function of PHP, which is used to determine if the variable is set or not.
	if(isset($_GET['index'])){
		
		// Store the value of "index" in a variable
		$indexGet = $_GET['index'];

		echo "The retrieve task from GET is $tasks[$indexGet]";

	}

	if(isset($_POST['index'])){
		
		// Store the value of "index" in a variable
		$indexPost = $_POST['index'];

		echo "The retrieve task from POST is $tasks[$indexPost]";

	}

	

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Client-Server Communication (GET and POST)</title>
	</head>
	<body>
		<h1>Task index from GET</h1>
		
		<!-- This is used to request resources from the server. -->
		<form method="GET">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">GET</button>
		</form>

		<h1>Task index from POST</h1>
		
		<!-- This is used to send data to a server for further processing(add, update, delete). -->
		<form method="POST">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>

			<button type="submit">POST</button>
		</form>
	</body>
</html>