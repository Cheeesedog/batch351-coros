<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Activity</title>
	</head>
	<body>

		<?php session_start(); ?>

		<?php if(isset($_SESSION['email'])){?>

			<p>Hello, <?= $_SESSION['email'];?></p>

		<?php } else { ?>

			<form method="POST" action="./server.php">
					<input type="hidden" name="action" value="login" />
					Email: <input type="text" name="email" required />
					Password: <input type="password" name="password" required />
					<button type="submit">Login</button>
				</form>

			<?php if(isset($_SESSION['login_error_message'])):?>

				<?= $_SESSION['login_error_message']; ?>

			<?php endif; ?>

		<?php }; ?>
	</body>
</html>